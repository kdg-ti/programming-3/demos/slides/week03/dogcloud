package be.kdg.programming3.dogcloud.repository;

import be.kdg.programming3.dogcloud.domain.Dog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ListDogRepository implements DogRepository{
    private Logger logger = LoggerFactory.getLogger(ListDogRepository.class);
    private static List<Dog> dogs = new ArrayList<>();
    @Override
    public List<Dog> readDogs() {
        logger.info("Reading dogs from database...");
        return dogs;
    }

    @Override
    public Dog createDog(Dog dog) {
        dog.setId(dogs.size());
        logger.info("Creating dog {}", dog);
        dogs.add(dog);
        return dog;
    }
}
