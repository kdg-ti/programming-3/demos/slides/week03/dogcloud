package be.kdg.programming3.dogcloud.config;

import be.kdg.programming3.dogcloud.domain.Dog;
import be.kdg.programming3.dogcloud.domain.DogType;
import be.kdg.programming3.dogcloud.repository.DogRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Random;
import java.util.stream.Stream;

@Component
public class DogSeeder implements CommandLineRunner {
    private DogRepository dogRepository;

    public DogSeeder(DogRepository dogRepository) {
        this.dogRepository = dogRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        Random random = new Random();
        Stream.generate(()->new Dog("pluto" + random.nextInt(100),
                DogType.values()[random.nextInt(DogType.values().length)]))
                .limit(10).forEach(dogRepository::createDog);
    }
}
