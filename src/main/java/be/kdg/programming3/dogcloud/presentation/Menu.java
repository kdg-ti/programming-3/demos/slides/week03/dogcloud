package be.kdg.programming3.dogcloud.presentation;

import be.kdg.programming3.dogcloud.service.DogService;
import org.springframework.stereotype.Component;

@Component
public class Menu {
    private DogService dogService;

    public Menu(DogService dogService) {
        this.dogService = dogService;
    }

    public void show() {
        System.out.println("List of all dogs:");
        dogService.getDogs().forEach(System.out::println);
    }
}
