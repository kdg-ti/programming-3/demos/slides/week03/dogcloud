package be.kdg.programming3.dogcloud.service;

import be.kdg.programming3.dogcloud.domain.Dog;
import be.kdg.programming3.dogcloud.domain.DogType;

import java.util.List;

public interface DogService {
    List<Dog> getDogs();
    Dog addDog(String name, DogType type);
    Dog addDog(Dog dog);
}
