package be.kdg.programming3.dogcloud.service;

import be.kdg.programming3.dogcloud.domain.Dog;
import be.kdg.programming3.dogcloud.domain.DogType;
import be.kdg.programming3.dogcloud.repository.DogRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DogServiceImpl implements DogService{
    private Logger logger = LoggerFactory.getLogger(DogServiceImpl.class);
    private DogRepository dogRepository;

    public DogServiceImpl(DogRepository dogRepository) {
        logger.info("Creating DogRepository");
        this.dogRepository = dogRepository;
    }

    @Override
    public List<Dog> getDogs() {
        logger.info("Getting dogs...");
        return dogRepository.readDogs();
    }

    @Override
    public Dog addDog(String name, DogType type) {
        logger.info("Adding dog with name {} of type {}", name, type);
        return addDog(new Dog(name, type));
    }

    @Override
    public Dog addDog(Dog dog) {
        return dogRepository.createDog(dog);
    }
}
