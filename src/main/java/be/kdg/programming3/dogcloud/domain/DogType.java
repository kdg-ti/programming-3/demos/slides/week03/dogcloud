package be.kdg.programming3.dogcloud.domain;

public enum DogType {
    LABRADOR, TECKEL, GERMAN_SHEPARD,POODLE
}
