package be.kdg.programming3.dogcloud.domain;

public class Dog {
    private int id;
    private String name;
    private DogType dogType;

    public Dog(String name, DogType dogType) {
        this.name = name;
        this.dogType = dogType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DogType getDogType() {
        return dogType;
    }

    public void setDogType(DogType dogType) {
        this.dogType = dogType;
    }

    @Override
    public String toString() {
        return "Dog{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", dogType=" + dogType +
                '}';
    }
}
