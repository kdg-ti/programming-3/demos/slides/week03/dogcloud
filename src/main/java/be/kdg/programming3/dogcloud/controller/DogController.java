package be.kdg.programming3.dogcloud.controller;

import be.kdg.programming3.dogcloud.domain.Dog;
import be.kdg.programming3.dogcloud.domain.DogType;
import be.kdg.programming3.dogcloud.service.DogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class DogController {

	private final DogService dogService;
	private static final Logger LOG = LoggerFactory.getLogger(DogController.class);

	public DogController(DogService svc) {
		dogService=svc;
	}

	@GetMapping("grid")
	private String toGrid(){
		return "grid";
	}

	@GetMapping("/dogs")
	public String showDogs(Model model){
		List<Dog> dogs = dogService.getDogs();
		model.addAttribute("dogs", dogs);
		LOG.info("all dogs: {}", dogs);
		return "dog-list";
	}

	@PostMapping("/addDog")
	public String addDog(Dog dog){
		LOG.debug("Posting a dog: " + dog);
		dogService.addDog(dog);
		return "redirect:dogs";
	}

	@GetMapping("/addDog")
	public String addDogForm(Model model){
		LOG.debug("Showing a dog form");
		model.addAttribute("breeds", DogType.values());
		return "add-dog";
	}
}
