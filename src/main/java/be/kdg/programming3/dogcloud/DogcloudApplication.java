package be.kdg.programming3.dogcloud;

import be.kdg.programming3.dogcloud.presentation.Menu;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class DogcloudApplication {

    public static void main(String[] args) {
        var context = SpringApplication.run(DogcloudApplication.class, args);
//        context.getBean(Menu.class).show();
//        context.close();
    }

}
